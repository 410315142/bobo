﻿using System.Web.Mvc;

namespace WebApplication5.Areas.areas
{
    public class areasAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "areas";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "areas_default",
                "areas/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}